#!/bin/bash
#
# setmacdisplay.bash
# Overwrite a Remmina configuration file to point at
# a static IP address given a known MAC address.
#
# This allows us to run a Remmina display to a unit assigned an address
# via DHCP even when there is no name server.
#
# Known Issues:
# - Will overwrite *all* entries in a Remmina configuration file.
#

# Tell users the usage
display_usage() { 
	echo ""
	echo "setmacdisplay.bash MAC_ADDR" 
	echo "    Overwrite a Remmina configuration file to point at"
	echo "    an IP address given a known MAC address. "
	echo ""
	echo "    MAC_ADDR is the MAC address in colon delimited"
	echo "    format (i.e. 0a:0b:0c:0d:0e:0f)."
	echo ""
	echo "    WARNING: This script will overwrite *all* entries "
	echo "    in a Remmina configuration file."
	echo ""
	} 


# Check whether user had supplied -h or --help . If yes display usage 
	if [[ ( $@ == "--help") ||  $@ == "-h" ]] 
	then 
		display_usage
		exit 0
	fi 

# Grab MAC address from command line or use a known default.
	if [  $# -le 0 ] 
	then 
		# No parameter - use a default MAC address
		MAC_ADDR="dc:a6:32:5d:24:bc"
	else
		# Use parameter
		MAC_ADDR=$1
	fi
#
# Get IP address
MY_IP_ADDR=$(ip -o -f inet addr show | awk '/scope global/ {print $4}')

PI_IP_ADDR=$(nmap -sP $MY_IP_ADDR > /dev/null && arp -an | grep $MAC_ADDR | awk '{print $2}' | sed 's/[()]//g')
#
# Find Remmina configuration file.
CONFIG_FILE=$(find ~ -name '*.remmina')
#
# Tell user what we're doing...
echo "Setting Remmina IP address for MAC address $MAC_ADDR to $PI_IP_ADDR in config file $CONFIG_FILE."
#
# Substitute the 'server=ADDRESS' line with our aIP address
sed -i "s/^server=.*$/server=$PI_IP_ADDR/g" $CONFIG_FILE



