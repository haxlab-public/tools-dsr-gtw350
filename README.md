# README #

This repository contains a set of Python scripts to be used
for the setup of DSR GTW350 IoT gateway.
NetworkJoin.sh    -   To prepare the gateway for network joining
NetworkLeave.sh   -   To remove the gateway from the network at 
                      the conclusion of the hack session
ResetBulb.sh      -   To reset the Zigbee Sengled to factory settings.
